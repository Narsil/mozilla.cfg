//
// ********************************************************************************
//
// local-settings.js | Firefox
//
// ********************************************************************************
//
// Startup
// >>>>>>>>>>>>>>>>>>>>>
// Load the config. file
pref("general.config.filename", "mozilla.cfg");
// -------------------------------------
// Do not obscure the content with ROT-13
pref("general.config.obscure_value", 0);


