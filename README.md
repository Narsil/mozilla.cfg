## Best Use:

These files has been designed exclusively for Firefox Desktop.



## Settings Protection:

Important settings are enforced/locked within `mozilla.cfg`, those settings cannot be changed by addons/updates/Firefox or unwanted/accidental manipulation; To change those settings you can easily do it by editing `mozilla.cfg`.



## Installation:

### Windows

- Download and install the last version of Firefox release
- Clone or download zip file and extract it
- Locate Firefox's installation directory (where the firefox.exe is located) `C:\Program Files\Mozilla Firefox\` or `C:\Program Files (x86)\Mozilla Firefox\`
- Copy the files contained in `config` folder to the install directory
- Start Firefox and test if config was applied browsing to `about:config`.

### Linux

- Download and extract the last version of Firefox release
- Clone or download zip file and extract it
- Locate Firefox's installation directory `/usr/lib/firefox/`
- Copy the files contained in `config` folder to the install directory
- You can use directly Firefox by running `firefox/firefox`
- You can as well create a shortcut to `firefox/firefox` to open Firefox easily
- Start Firefox and test if config was applied browsing to `about:config`.

### Mac

- Download and install the last version of Firefox
- Clone or download zip file and extract it
- Locate Firefox's installation directory `Applications/Firefox.app/Contents/Resources/`
- Copy the files contained in `config` folder to the install directory
- Start Firefox and test if config was applied browsing to `about:config`.


### Uninstall:

- To uninstall, just remove the files you added to your Firefox's install directory; then restart Firefox.

### BUGS:

- Enabling a string with the value defaultPref("browser.contentblocking.category", "strict/standard/custom"); doesn't work and breaks TCP.
Nevertheless, by using LockPref instead works. Anyway, I prefer not locking this, for example if you want to block all cookies.


## Thanks:

* [arkenfox](https://github.com/arkenfox/user.js) - The mayority content of this mozilla.cfg.
* [QUINDECIM](https://git.nixnet.services/quindecim/mozilla.cfg) - Original mozilla.cfg and design.
* [Librewolf](https://librewolf.net) - Some strings.
* [celenity](https://codeberg.org/celenity/Phoenix) - More strings.